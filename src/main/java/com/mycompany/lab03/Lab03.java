/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab03;

import java.util.Scanner;

/**
 *
 * @author Leywin
 */
public class Lab03 {
    
    static char[][] table = {{'-', '-', '-'},{'-', '-', '-'},{'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int row, col;
    
    public static void main(String[] args) {
        printWelcome();
        while(true) {
            printTable();
            printTurn();
            inputRowCol();
            if(isWin()) {
                printTable();
                printWin();
                break;
            }
            switchPlayer();
        }
    }

    private static void printWelcome() {
        System.out.println("Welcome to XO Game");
    }

    private static void printTable() {
        for(int i=0; i<3; i++) {
            for(int j=0; j<3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " turn");
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.print("Please input row, col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if(table[row-1][col-1]=='-'){
                table[row-1][col-1] = currentPlayer;
                break;
            }
            System.out.println("Tie!!You can't continue.");
        }
    }

    private static void switchPlayer() {
        if(currentPlayer=='X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static boolean isWin() {
        if(checkRow()) {
            return true;
        }
        if(checkCol()) {
            return true;
        }
        if(checkTie()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println(currentPlayer + " Won!!!");
    }

    private static boolean checkRow() {
        for(int i=0; i<3; i++) {
            if(table[row-1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for(int i=0; i<3; i++) {
            if(table[i][col-1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkTie() {
        if (table[1-1][1-1] == currentPlayer && table[2-1][2-1] == currentPlayer && table[3-1][3-1] == currentPlayer
            || table[1-1][3-1] == currentPlayer && table[2-1][2-1] == currentPlayer && table[3-1][1-1] == currentPlayer)
        {
           return true;
        }
        return false;
        }

}
